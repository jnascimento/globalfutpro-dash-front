
const __PROTOCOL__ = 'http://';
const __PORT_API__ = '3000';

const SERVER =  '142.93.123.145';
const LOCAL = 'localhost';

const DOMAIN_SERVER = __PROTOCOL__ + 'www.globalfut.pro';
const DOMAIN_LOCAL = __PROTOCOL__ + 'localhost';

const IP = SERVER;
const DOMAIN = DOMAIN_SERVER; 

window.LIB = {};
window.LIB.PROTOCOL = __PROTOCOL__;
window.LIB.IP = IP;
window.LIB.DOMAIN = DOMAIN;
window.LIB.PORT_API = __PORT_API__;

//<span class="footer-btn-text">HOME</span>
// <span class="footer-btn-text">MEU PERFIL</span>

window.getMenu = (id = 0) => `
<nav class="row col-2">
<section class="col footer-menu">
    <a href="/logout" class="menu-link">
    <button class="btn-home">
      <img src="../assets/img/icons/png-red/005-exit-bold.png" alt="Botão para SAIR DO SISTEMA">
    </button>
  </a>
</section>
  <section class="col footer-menu">
      <a href="/dashboard" class="menu-link">
      <button class="btn-home">
        <img src="../assets/img/icons/png-red/043-home.png" alt="Botão para ir para a HOME">
      </button>
    </a>
  </section>
  <section class="col footer-menu">
    <a href="/meu_perfil" class="menu-link">
      <button class="btn-perfil">
        
        <img src="../assets/img/icons/png-red/008-friend-bold.png" alt="Botão para ir para o seu PERFIL">
      </button>
    </a>
  </section>
</nav>
`;

window.getheader = () => `
<section class="col box-header">
  <img class="logo" src="../assets/img/logo-red3.png" alt="">

  <img src="../assets/img/beta-badge.png" alt="BETA" id="badge-beta">
  <!-- <a href="/logout" class="box-header-logout-link">
    <img src="../assets/img/icons/png/005-exit-suissa.png" alt="Sair do SISTEMA" class="box-header-logout-icon">
  </a> -->
</section>
`;
