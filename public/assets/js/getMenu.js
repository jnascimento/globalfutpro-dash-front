
const PROTOCOL = 'http://';
const PORT_API = '3000';

const SERVER =  '142.93.123.145';
const LOCAL = 'localhost';

const DOMAIN_SERVER = PROTOCOL + 'www.globalfut.pro';
const DOMAIN_LOCAL = PROTOCOL + 'localhost';

const IP = SERVER;
const DOMAIN = DOMAIN_SERVER; 

window.LIB = {};
window.LIB.PROTOCOL = PROTOCOL;
window.LIB.IP = IP;
window.LIB.DOMAIN = DOMAIN;
window.LIB.PORT_API = PORT_API;

window.getMenu = (id = 0) => `
<ul class="nav">
  <li>
    <a href="/campeonatos">
      <i class="now-ui-icons sport_trophy"></i>
      <p>Campeonatos</p>
    </a>
  </li> 
  <li>
  <a href="/rankings">
    <i class="now-ui-icons design_app"></i>
    <p>Ranking</p>
  </a>
  </li> 
  <li>
  <a href="/os_melhores">
    <i class="now-ui-icons design_app"></i>
    <p>Os MELHORES</p>
  </a>
  </li> 
  <li>
    <a href="/regulamento">
      <i class="now-ui-icons files_paper"></i>
      <p>Regulamento</p>
    </a>
  </li> 
  <li>
    <a href="/mercado_da_bola">
      <i class="now-ui-icons business_globe"></i>
      <p>Mercado da Bola</p>
    </a>
  </li> 
  <li>
  <a href="/meu_perfil">
    <i class="now-ui-icons design_app"></i>
    <p>Meu Perfil</p>
  </a>
  </li> 
  <li>
  <a href="/logout">
    <i class="now-ui-icons design_app"></i>
    <p>Logout</p>
  </a>
  </li> 
</ul>
`;




/**
<li>
<a href="/jogador/${id}">
  <i class="now-ui-icons sport_trophy"></i>
  <p>HOME</p>
</a>
</li> 
 */


/**
<li>
<a href="/meu_perfil/${id}">
  <i class="now-ui-icons design_app"></i>
  <p>Perfil</p>
</a>
</li> 
*/