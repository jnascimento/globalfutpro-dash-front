
	const getConsole = (player) => {
		if (player.name_psn !== "Nome na PSN")
			return "psn";
		if (player.name_xbox !== "Nome na XBOX")
			return "xbox";
		if (player.name_pc !== "Nome na PC")
			return "pc";
	}

(function($) {
  "use strict";

  console.log('LIBBBEEEE: ', {LIB})

  $('.icon-close').click(() => { 
    $('#wrapper-login-form').addClass('hide');
    $('.show-form').removeClass('show-form');
  });

  $('#btn-cadastrar').hide();


  $('#btn-entrar').click(function () {
    console.log('entrar')
    
    $('#wrapper-login-form').removeClass('hide', () => {
      
    });
    // $('#title-top').addClass('hashtag-top');

    $('.header-content').addClass('show-form');


    $('[name=email]').blur(async function() {

      const email = $("[name=email]").val();

      if (email.trim() === '')
        return false;

      const data = {
        email
      }

      console.log({data});

      // const url = 'http://www.globalfut.pro:3000/api/users/login';
      const url = `http://${LIB.IP}:3000/api/users/login/email`;
      const teamSUBMIT = await jQuery.ajax({
        type: 'POST', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.
        dataType: 'json', // Set datatype - affects Accept header
        url, // A valid URL
        headers: {"X-HTTP-Method-Override": "POST"}, // X-HTTP-Method-Override set to PUT.
        data: data // Some data e.g. Valid JSON as a string
      });
      
      console.log({teamSUBMIT});

      if (!teamSUBMIT) {
        $('#btn-login').fadeOut(100, function(){
          
          $('#btn-cadastrar').show();
          $('#btn-cadastrar').addClass("wobble");
        });
        
        // $('#btn-login').attr('disabled', true);
        // $('#btn-cadastrar').attr('disabled', false);

        alert("Seu EMAIL não existe! Favor cadastrar-se.");
        
      }
      else {
        $('#btn-login').show();
        $('#btn-cadastrar').fadeOut('slow');
        $('#btn-login').addClass("wobble");
        // $('#btn-login').show();
        // $('#btn-cadastrar').addClass('hide');
        // $('#btn-cadastrar').addClass('btn-disabled');
        // $('#btn-login').removeClass('btn-disabled');

        // $('#btn-cadastrar').attr('disabled', true);
        // $('#btn-login').attr('disabled', false);
      }
    });


  });

  $('body').scrollspy({
      target: '.navbar-fixed-top',
      offset: 60
  });

  $('#topNav').affix({
      offset: {
          top: 200
      }
  });
  
  new WOW().init();
  
  $('a.page-scroll').bind('click', function(event) {
      var $ele = $(this);
      $('html, body').stop().animate({
          scrollTop: ($($ele.attr('href')).offset().top - 60)
      }, 1450, 'easeInOutExpo');
      event.preventDefault();
  });
  
  $('.navbar-collapse ul li a').click(function() {
      /* always close responsive nav after click */
      $('.navbar-toggle:visible').click();
  });

  $('#galleryModal').on('show.bs.modal', function (e) {
      $('#galleryImage').attr("src",$(e.relatedTarget).data("src"));
  });


  $(document).on('click dblclick', '#btn-login', async( e ) => {
    // return true;
    const email = $("[name=email]").val();
    const password = $("[name=password]").val();

    const data = {
      email,
      password
    }

    console.log({data});

    const url = `http://${LIB.IP}:3000/api/users/login/password`;
    const teamSUBMIT2 = await jQuery.ajax({
      type: 'POST',
      dataType: 'json',
      url,
      headers: {"X-HTTP-Method-Override": "POST"},
      data
    });
    
    console.log({teamSUBMIT2});
  
    if (teamSUBMIT2) {
      const __console = getConsole(teamSUBMIT2);

      localStorage.setItem('jogadorID', teamSUBMIT2._id);
      localStorage.setItem('consoleID', __console);
      localStorage.setItem('teamID', teamSUBMIT2.team);
      console.log(
        'localstorage: ',
        localStorage.getItem('jogadorID'),
        localStorage.getItem('consoleID'),
        localStorage.getItem('teamID')
      )
      return window.location = `${LIB.DOMAIN}/dashboard`;
    }
    else {
      alert("Email e/ou senha INVÁLIDOS!");
      // const ALERT_TITLE = `ERRO`;
      // const ALERT = `<strong>SENHA INVÁLIDA!!!!</strong>`;
      
      // $("#alert .popup h2").text(ALERT_TITLE);
      // $("#alert .popup h2").addClass("alert-title-error");
      // $("#alert .content").html(ALERT);
      // return $("#alert").addClass("alert-show");
      // return alert("SENHA INVÁLIDA!!!!")
    }
  });


  $(document).on('click dblclick', '#btn-cadastrar', async( e ) => {
    const email = $("[name=email]").val();
    const password = $("[name=password]").val();

    const data = {
      email,
      password
    }

    console.log({data});
    
    const url = `http://${LIB.IP}:3000/api/users/`;
    const teamSUBMIT = await jQuery.ajax({
      type: 'POST',
      dataType: 'json',
      url, // A valid URL
      headers: {"X-HTTP-Method-Override": "POST"},
      data
    });
    
    console.log({teamSUBMIT});

    if (teamSUBMIT) {
      alert("Usuário cadastrado COM SUCESSO!")
      // window.location = `${LIB.DOMAIN}/`;
      
      // $('#btn-cadastrar').addClass('hide');
      // $('#btn-login').fade('hide');
      
      $('#btn-cadastrar').fadeOut(100, function(){
        
        $('#btn-login').show("slow", function(){
          $('#btn-login').addClass("wobble");
        });
      });
    }
    else {
      return alert("Usuário NÃO pode ser cadastrado!")
    }
  });

})(jQuery);