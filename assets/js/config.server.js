
const PROTOCOL = 'http://';
const PORT_API = '3000';

const SERVER =  '142.93.123.145';
const LOCAL = 'localhost';

const DOMAIN_SERVER = PROTOCOL + 'www.globalfut.pro';
const DOMAIN_LOCAL = PROTOCOL + 'localhost';

const IP = LOCAL;
const DOMAIN = DOMAIN_LOCAL; 

window.LIB = {};
window.LIB.PROTOCOL = PROTOCOL;
window.LIB.IP = IP;
window.LIB.DOMAIN = DOMAIN;
window.LIB.PORT_API = PORT_API;
