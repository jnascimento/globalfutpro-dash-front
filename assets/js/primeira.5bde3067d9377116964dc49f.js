{
	"_id": "5bde3067d9377116964dc49f",
	"name": "1º Global Division",
	"type": "liga",
	"teams": [
		{
			"_id": "5bde30df2aa35816dae91414",
			"name": "TROQUEI",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91419",
			"name": "TIME F",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141e",
			"name": "TIME K",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91423",
			"name": "TIME P",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91417",
			"name": "TIME D",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141c",
			"name": "TIME I",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91421",
			"name": "TIME N",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91426",
			"name": "TIME S",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91422",
			"name": "TIME O",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91418",
			"name": "TIME E",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91427",
			"name": "TIME T",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141d",
			"name": "TIME J",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141b",
			"name": "TIME H",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91416",
			"name": "TIME C",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91420",
			"name": "TIME M",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91425",
			"name": "TIME R",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91415",
			"name": "TIME B",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae91424",
			"name": "TIME Q",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141f",
			"name": "TIME L",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		},
		{
			"_id": "5bde30df2aa35816dae9141a",
			"name": "TIME G",
			"badge": "default",
			"division": "primeira",
			"overall": 70,
			"players": [],
			"__v": 0
		}
	],
	"games": [],
	"__v": 0,
	"sequence": [
		[
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		],
		[
			[
				{
					"_id": "5bde30df2aa35816dae91419",
					"name": "TIME F",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91414",
					"name": "TROQUEI",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91427",
					"name": "TIME T",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141e",
					"name": "TIME K",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141d",
					"name": "TIME J",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91423",
					"name": "TIME P",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141b",
					"name": "TIME H",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91417",
					"name": "TIME D",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91416",
					"name": "TIME C",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141c",
					"name": "TIME I",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91420",
					"name": "TIME M",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91421",
					"name": "TIME N",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91425",
					"name": "TIME R",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91426",
					"name": "TIME S",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91415",
					"name": "TIME B",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91422",
					"name": "TIME O",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae91424",
					"name": "TIME Q",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae91418",
					"name": "TIME E",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			],
			[
				{
					"_id": "5bde30df2aa35816dae9141f",
					"name": "TIME L",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				},
				{
					"_id": "5bde30df2aa35816dae9141a",
					"name": "TIME G",
					"badge": "default",
					"division": "primeira",
					"overall": 70,
					"players": [],
					"__v": 0
				}
			]
		]
	]
}